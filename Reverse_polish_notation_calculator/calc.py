import re

first_operation_regex = re.compile('(\d+\.?\d*)\s(\d+\.?\d*)\s\W')

def calc(expr: str) -> float:
    if expr == '':
        result = 0
    else:
        try:
            result = float(expr)
        except ValueError:
            expr = replace_first_operation(expr)
            result = calc(expr)
    return result

def replace_first_operation(expr: str) -> str:
    return first_operation_regex.sub(evaluate_operation, expr, 1)

def evaluate_operation(expr: re.Match) -> str:
    content = expr.group().split()
    equation = ''.join((content[0], content[2], content[1]))
    result = eval(equation)
    return str(result)
