import pytest

from calc import calc, replace_first_operation, evaluate_operation, \
first_operation_regex

class TestCalc:
    def test_empty_string(self):
        assert calc('') == 0

    def test_parses_int(self):
        assert calc('3') == 3

    def test_parses_float(self):
        assert calc('2.5') == 2.5

    def test_single_operation(self):
        assert calc('1 2 +') == 3

    def test_two_operations(self):
        assert calc('3 1 2 + *') == 9

class TestReplaceFirstOperation:

    def test_single_operation(self):
        assert replace_first_operation('1 2 +') == '3'

class TestEvaluateOperation:

    def test_addition(self):
        match = first_operation_regex.search('1 2 +')
        assert evaluate_operation(match) == '3'

class TestRegex:

    def test_single_operation_only_ints(self):
        assert first_operation_regex.search('1 2 +').group() == '1 2 +'

    def test_single_operation_int_and_float(self):
        assert first_operation_regex.search('1 2.5 +').group() == '1 2.5 +'

    def test_single_operation_two_floats(self):
        assert first_operation_regex.search('1.5 2.5 +'),group() == '1.5 2.5 +'

    def test_two_operations(self):
        assert first_operation_regex.search('3 1 2.5 + *').group() == '1 2.5 +'
